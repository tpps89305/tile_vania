﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelExit : MonoBehaviour
{

    [SerializeField]
    float levelLoadDelay = 2f;
    [SerializeField]
    float timeSlowFactor = 0.5f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine(GoToNextLevel());
    }

    IEnumerator GoToNextLevel()
    {
        Time.timeScale = timeSlowFactor;
        yield return new WaitForSecondsRealtime(levelLoadDelay);
        Time.timeScale = 1;
        // 讀取下一關
        foreach (ScenePersistance scenePersistance in FindObjectsOfType<ScenePersistance>())
        {
            Destroy(scenePersistance);
        }
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }

}
