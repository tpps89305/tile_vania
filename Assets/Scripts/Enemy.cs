﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    [SerializeField]
    float moveSpeed = 1f;
    private Rigidbody2D mRigidbody;
    private BoxCollider2D mFootCollider;

    // Start is called before the first frame update
    void Start()
    {
        mRigidbody = GetComponent<Rigidbody2D>();
        mFootCollider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (IsFaceingRight())
        {
            mRigidbody.velocity = new Vector2(moveSpeed, 0f);
        } else
        {
            mRigidbody.velocity = new Vector2(-moveSpeed, 0f);
        }
    }

    private bool IsFaceingRight()
    {
        return transform.localScale.x > 0;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("OnTriggerExit2D: " + collision);
        transform.localScale = new Vector2(-(Mathf.Sign(mRigidbody.velocity.x)), 1f);
    }
    
}
