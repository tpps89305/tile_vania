﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSession : MonoBehaviour
{
    [SerializeField] int playerLife = 3;
    [SerializeField] int score = 0;
    [SerializeField] Text textPlayerLifes;
    [SerializeField] Text textCoin;

    private void Awake()
    {
        int mGameSession = FindObjectsOfType<GameSession>().Length;
        if (mGameSession > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        textPlayerLifes.text = playerLife.ToString();
        textCoin.text = score.ToString();
    }

    public void AddToScore()
    {
        score += 100;
        textCoin.text = score.ToString();
    }

    public void ProcessPlayerDeath()
    {
        if (playerLife > 1)
        {
            TakeLife();
        }
        else
        {
            ResetGameSession();
        }
    }

    private void ResetGameSession()
    {
        SceneManager.LoadScene(0);
        Destroy(gameObject);
    }

    private void TakeLife()
    {
        playerLife--;
        Debug.Log("TakeLife, Player Lifes = " + playerLife);
        // 重新讀取場景
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
        textPlayerLifes.text = playerLife.ToString();
    }

}
