﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickup : MonoBehaviour
{

    [SerializeField]
    AudioClip coinPickupSFX;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // 避免因角色設有多個碰撞偵測器，造成重覆執行程序的現象。
        if (collision is CapsuleCollider2D)
        {
            AudioSource.PlayClipAtPoint(coinPickupSFX, Camera.main.transform.position);
            Destroy(gameObject);
            FindObjectOfType<GameSession>().AddToScore();
        }

    }
}
