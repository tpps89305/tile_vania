﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour
{
    [SerializeField]
    float runSpeed = 5f;
    [SerializeField]
    float jumpSpeed = 5f;
    [SerializeField]
    float climbSpeed = 5f;
    [SerializeField]
    AudioClip jumpSFX;
    [SerializeField]
    AudioClip hurtSFX;

    bool isAlive = true;

    private Rigidbody2D mRigidbody;
    private Animator mAnimator;
    private CapsuleCollider2D mBodyCollider2D;
    private BoxCollider2D mFootCollider2D;
    private float mGravityScale;
    private Vector2 deathKick = new Vector2(25, 25);

    // Start is called before the first frame update
    void Start()
    {
        mRigidbody = GetComponent<Rigidbody2D>();
        mAnimator = GetComponent<Animator>();
        mBodyCollider2D = GetComponent<CapsuleCollider2D>();
        mGravityScale = mRigidbody.gravityScale;
        mFootCollider2D = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            Run();
            Jump();
            FlipSprite();
            Climb();
            Die();
        }
    }

    /// <summary>
    /// 使人物移動
    /// </summary>
    private void Run()
    {
        float controlThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        Vector2 playerVelocity = new Vector2(controlThrow * runSpeed, mRigidbody.velocity.y);
        mRigidbody.velocity = playerVelocity;

        if (Mathf.Abs(controlThrow) > Mathf.Epsilon)
        {
            mAnimator.SetBool("Running", true);
        }
        else
        {
            mAnimator.SetBool("Running", false);
        }

    }

    /// <summary>
    /// 翻轉貼圖。當人物往另一個方向移動時，將貼圖水平翻轉。
    /// </summary>
    private void FlipSprite()
    {
        //transform.localScale = new Vector2(Mathf.Sign(mRigidbody.velocity.x), 1f);
        // Mathf.Epsilon 是一個趨近於零的小數
        if (Mathf.Abs(mRigidbody.velocity.x) > Mathf.Epsilon)
        {
            transform.localScale = new Vector2(Mathf.Sign(mRigidbody.velocity.x), 1f);
        }
    }

    /// <summary>
    /// 使人物跳躍
    /// </summary>
    private void Jump()
    {
        if (CrossPlatformInputManager.GetButtonDown("Jump") && mFootCollider2D.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            Vector2 jumpVelocityToAdd = new Vector2(0f, jumpSpeed);
            mRigidbody.velocity += jumpVelocityToAdd;
            AudioSource.PlayClipAtPoint(jumpSFX, Camera.main.transform.position);
        }
    }

    /// <summary>
    /// 使人物攀爬
    /// </summary>
    private void Climb()
    {
        if (mBodyCollider2D.IsTouchingLayers(LayerMask.GetMask("Climb")))
        {
            float controlThrow = CrossPlatformInputManager.GetAxis("Vertical");
            Vector2 climbVelocityToAdd = new Vector2(mRigidbody.velocity.x, controlThrow * climbSpeed);
            mRigidbody.velocity = climbVelocityToAdd;
            mRigidbody.gravityScale = 0f;
            mAnimator.SetBool("Climbing", true);
        } else
        {
            mRigidbody.gravityScale = mGravityScale;
            mAnimator.SetBool("Climbing", false);
        }
        //mAnimator.SetBool("Climbing", Mathf.Abs(mRigidbody.velocity.y) > Mathf.Epsilon);
    }

    private void Die()
    {
        if (mBodyCollider2D.IsTouchingLayers(LayerMask.GetMask("Emeny")) || mBodyCollider2D.IsTouchingLayers(LayerMask.GetMask("Trap")))
        {
            Vector2 playerVelocity = new Vector2(0, mRigidbody.velocity.y);
            mRigidbody.velocity = playerVelocity;
            mAnimator.SetTrigger("Die");
            isAlive = false;
            AudioSource.PlayClipAtPoint(hurtSFX, gameObject.transform.position);
            Debug.Log("角色陣亡");
            //mRigidbody.velocity = deathKick;
            //FindObjectOfType<GameSession>().ProcessPlayerDeath();
            StartCoroutine(ToDie());
        }
    }

    IEnumerator ToDie()
    {
        yield return new WaitForSecondsRealtime(3f);
        FindObjectOfType<GameSession>().ProcessPlayerDeath();
    }
}
